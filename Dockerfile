FROM rust:1.54-slim-bullseye AS builder
WORKDIR /usr/src/blog
COPY . .
# --root . --path backend tells cargo install to put our backend rocket server's
# binary in a in /bin which will be in /usr/src/blog/bin (see below)
RUN apt update && \
apt install -y pkg-config libssl-dev && \
cargo install --root . --path backend && \
rm -R target

FROM debian:bullseye-slim
WORKDIR /usr/src/blog
COPY --from=builder /usr/src/blog .
RUN apt update && \
apt install -y pkg-config libssl-dev
# tells the system to look for our "blog" cmd inside the bin in our blog folder
ENV PATH=$PATH:/usr/src/blog/bin
ENV ROCKET_ADDRESS="0.0.0.0"
# debug during development cycle.
ENV ROCKET_LOGLEVEL="Debug"
CMD ["blog"]