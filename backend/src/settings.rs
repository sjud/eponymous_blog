use std::env;
use std::env::VarError;
use rocket::request::{FromRequest, Outcome};
use rocket::Request;
use rocket::http::Status;
/// Our server's global settings structure.
#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub struct Settings {
    pub postgres: PostgresSettings,
    pub gitlab: GitlabSettings,
}
/// The server Setting's Postgres specific sub-structure.
#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub struct PostgresSettings {
    pub db: String,
    pub user: String,
    pub password: String,
    pub host_auth_method: String,
}
/// For responding to gitlab webhooks using the gitlab api library.
#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub struct GitlabSettings {
    pub private_token: String,
    pub x_gitlab_token_pipeline: GitlabPipelineEventToken,
    pub production_branch_title: String,
    pub deployment_branch_title: String,
    pub gitlab_url: String,
}
#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub struct GitlabPipelineEventToken(String);

#[rocket::async_trait]
impl<'r> FromRequest<'r> for GitlabPipelineEventToken {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        // Get our settings from our rocket's managed state.
        let settings: &Settings = request.rocket().state().expect("Program can't be initialized without \
        settings.");
        // Find what value we're expecting from the X-Gitlab-Token header.
        let header_value_expected = &settings.gitlab.x_gitlab_token_pipeline.0;

        match request.headers().get("X-Gitlab-Token").next() {
            // If our expected header value equals our received value return Success(Self)
            Some(header_value) if header_value == header_value_expected => {
                Outcome::Success(GitlabPipelineEventToken(header_value.to_string()))
            },
            _ => {
                // Other no header || wrong value so return failure.
                Outcome::Failure((Status::Unauthorized,()))
            }
        }
    }
}
impl Settings {
    pub fn new() -> Result<Self, VarError> {
        let postgres = PostgresSettings {
            db: env::var("POSTGRES_DB")?,
            user: env::var("POSTGRES_USER")?,
            password: env::var("POSTGRES_PASSWORD")?,
            host_auth_method: env::var("POSTGRES_HOST_AUTH_METHOD")?,
        };
        let gitlab = GitlabSettings {
            private_token: env::var("GITLAB_PRIVATE_TOKEN")?,
            x_gitlab_token_pipeline: GitlabPipelineEventToken(
                env::var("GITLAB_PIPELINE_EVENT_TOKEN")?
            ),
            production_branch_title: env::var("GITLAB_PRODUCTION_BRANCH")?,
            deployment_branch_title: env::var("GITLAB_DEPLOYMENT_BRANCH")?,
            gitlab_url: env::var("GITLAB_URL")?,
        };
        Ok(Settings { postgres, gitlab })
    }
}
