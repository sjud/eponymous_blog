use crate::settings::{GitlabPipelineEventToken, Settings};
use rocket::http::Status;
use gitlab::webhooks::PipelineHook;
use rocket::serde::json::Json;
use rocket::State;
use gitlab::api::projects::merge_requests::{CreateMergeRequest, MergeMergeRequest};
use gitlab::types::MergeRequest;
use gitlab::api::{Query};
use gitlab::Gitlab;
use log::{error,debug,trace};

// if our tests pass and our branch name == production branch then send out a merge request with main
// and approve it.
#[post("/gitlab/pipeline_event", format = "json", data = "<pipeline_hook>")]
pub async fn gitlab_pipeline_event(
    _token: GitlabPipelineEventToken,
    pipeline_hook: Json<PipelineHook>,
    settings: &State<Settings>,
    gitlab_client: &State<Gitlab>,
) -> Result<Status,Status> {
    let hook_source = &pipeline_hook.object_attributes.source;
    let settings_source = &settings.gitlab.production_branch_title;
    // If our source branch doesn't equal our production branch return early.
    if hook_source != settings_source {
        debug!("Branch : {} is not production branch titled: {}",
               hook_source,
               settings_source
        );
        return Ok(Status::ExpectationFailed);
    }
    let project_id = pipeline_hook.project.id.value();
    let source_branch = &settings.gitlab.production_branch_title;
    let target_branch = &settings.gitlab.deployment_branch_title;
    // Auto MERGE
    let merge_request = create_merge_request(
        project_id,
        source_branch,
        target_branch,
        gitlab_client
    )
        .map_err(|e|{
            error!("{}",e);
            Status::UnprocessableEntity
    })?;

    merge_merge_request(&merge_request,gitlab_client)
        .map_err(|e|{
            error!("{}",e);
            Status::UnprocessableEntity
        })?;

    trace!("{} merged with {}",source_branch,target_branch);
    Ok(Status::Ok)
}

fn create_merge_request(project_id:u64,source:&String,target:&String, client: &Gitlab) -> Result<MergeRequest,String> {
    let create_merge_request: CreateMergeRequest = CreateMergeRequest::builder()
        .project(project_id)
        .source_branch(source)
        .target_branch(target)
        .title(format!("Auto merge:{}",source))
        .description("")
        .squash(true)
        .build()?;
    trace!("{:?}", &create_merge_request);
    let merge_request: MergeRequest = create_merge_request
        .query(client).map_err(|e|e.to_string())?;
    trace!("Successfully created merge request.");
    Ok(merge_request)
}

fn merge_merge_request(merge_request: &MergeRequest,client: &Gitlab) -> Result<(),String> {
    let merge_merge_request: MergeMergeRequest = MergeMergeRequest::builder()
        .project(*&merge_request.project_id.value())
        .merge_request(merge_request.id.value())
        .build()?;
    trace!("{:?}", &merge_merge_request);
    let _ = gitlab::api::ignore(merge_merge_request).query(client)
        .map_err(|e| e.to_string())?;
    trace!("Successfully merged merge requst.");
    Ok(())
}
