use rocket::http::Status;

#[get("/health")]
pub async fn health() -> Status {
    Status::Ok
}

#[cfg(test)]
mod tests {
    #[test]
    fn health_check() {
        use rocket::http::Status;
        use rocket::local::blocking::Client;
        let client = Client::tracked(
            rocket::build()
                .mount("/api",routes![crate::routes::health::health])
        ).expect("valid rocket instance");
        let response = client.get("/api/health").dispatch();
        assert_eq!(response.status(), Status::Ok);
    }
}