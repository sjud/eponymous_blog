mod routes;
mod settings;

#[macro_use]
extern crate rocket;

use crate::settings::Settings;
use gitlab::Gitlab;

/*
e.g.
fn thing1_routes() -> Vec<Route> {
    routes![a, b, c].map(|r| r.map_base(|b| "/api/thing1/{}".format(b)))
}
 */

#[launch]
fn rocket() -> _ {
    let settings = Settings::new().expect("Rocket launch expected settings on launch");
    let gitlab_client = Gitlab::new(
        &settings.gitlab.gitlab_url,
        &settings.gitlab.private_token
    ).expect("Can't access gitlab api without valid gitlab instance.");
    rocket::build()
        .mount("/api", routes![
        routes::health::health,
        routes::gitlab::auto_merge::gitlab_pipeline_event,
        ])
        .manage(settings)
        .manage(gitlab_client)
}
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert!(true)
    }

}
